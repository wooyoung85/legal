//form data serialize
jQuery.fn.serializeObject = function () {
    var obj = null;
    try {
        if (this[0].tagName && this[0].tagName.toUpperCase() == "FORM") {
            var arr = this.serializeArray();
            if (arr) {
                obj = {};
                jQuery.each(arr, function () {
                    obj[this.name] = this.value;
                });
            }
        }
    } catch (e) {
        alert(e.message);
    } finally {
    }
    return obj;
}

// fine-upload 설정
function getFineUploaderConfig(menuId, type) {
    var config = {}
    config.template = 'fine-upload-template';
    config.request = {
        endpoint: '/api/upload/' + menuId + '/' + type
    };
    config.deleteFile = {
        enabled: true,
        forceConfirm: true,
        endpoint: '/api/upload'
    };
    config.callbacks = {
        onDelete: function (id) {
            this.setDeleteFileParams({filename: this.getName(id)}, id);
        }
    };
    config.autoUpload = false;

    return config;
}

//모달 팝업 관련
function popupModal(label, message) {
    $('#modalMessageLabel').html(label)
    $('#modalMessageBody').html(message)

    $('#modalMessage').modal()

    $('#modalMessage').on('hidden.bs.modal', function (e) {
        $('#modalMessageLabel').html()
        $('#modalMessageBody').html()
    })
}

function convertErrorMessage(resp) {
    var errors = JSON.parse(resp)

    var convertedHtml = "<ul>";
    $.each(errors.error, function (index, item) {
        convertedHtml += '<li>' + item.defaultMessage + '</li>'
    });
    convertedHtml += "</ul>";

    return convertedHtml
}