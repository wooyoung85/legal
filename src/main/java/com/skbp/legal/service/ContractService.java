package com.skbp.legal.service;

import com.skbp.legal.domain.common.Member;
import com.skbp.legal.domain.contract.Contract;
import com.skbp.legal.domain.contract.ContractStatus;
import com.skbp.legal.form.ContractForm;
import com.skbp.legal.repository.ContractRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class ContractService {

    private final ContractRepository contractRepository;

    @Transactional
    public void createContractRequest(ContractForm form, Member currentUser){
        Contract contract = Contract.builder()
            .manageNumber(generateManageNumber())
            .requestMember(currentUser)
            .name(form.getName())
            .companyName(form.getCompanyName())
            .counterPartner(form.getCounterPartner())
            .startTermDate(form.getStartTermDT())
            .endTermDate(form.getEndTermDT())
            .contractCurrecy(form.getContractCurrecy())
            .contractPrice(form.getContractPrice())
            .contractLanguages(form.getContractLanguages())
            .standardContractUseYn(form.getStandardContractUseYn())
            .dueDate(form.getDueDate())
            .dealFromDate(form.getDealFromDate())
            .dealToDate(form.getDealToDate())
            .documentStatus(form.getDocumentStatus())
            .contents(form.getContents())
            .contentsHtml(form.getContentsHtml())
            .contractStatus(ContractStatus.STATUS0)
            .build();

        contractRepository.save(contract);
    }

    public Contract findOne(Long id) {
        return contractRepository.findOne(id);
    }

    public List<Contract> findContractRequests() {
        return contractRepository.findAll();
    }

    // 변경 감지로 업데이트 처리해야 함!
    // merge 기능 사용 X
    @Transactional
    public void updateItem(Long itemId, String name) {
        Contract contract = contractRepository.findOne(itemId);
        contract.change(name);
    }


    // 관리번호 생성(C-연도-5자리순번)
    private String generateManageNumber(){
        int topNumber = contractRepository.getMaxId();
        String number = String.format("%05d", topNumber + 1);
        String yyyy = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy"));
        return String.format("C%s-%s", yyyy, number);
    }

}
