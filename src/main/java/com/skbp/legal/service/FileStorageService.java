package com.skbp.legal.service;

import com.skbp.legal.common.StorageException;
import com.skbp.legal.config.FileStorageProperties;
import com.skbp.legal.controller.resource.UploadRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Service
public class FileStorageService {
    private static final Logger log = LoggerFactory.getLogger(FileStorageService.class);

    private Path basePath;

    @Autowired
    public FileStorageService(FileStorageProperties properties) {
        this.basePath = properties.getBaseDir();
    }

    public void save(UploadRequest ur) {

        if (ur.getFile().isEmpty()) {
            throw new StorageException(String.format("File with uuid = [%s] is empty", ur.getUuid().toString()));
        }

        Path targetFile;
        if (ur.getPartIndex() > -1) {
            targetFile = basePath.resolve(ur.getUuid()).resolve(String.format("%s_%05d", ur.getUuid(), ur.getPartIndex()));
        } else {
            targetFile = basePath.resolve(ur.getUuid()).resolve(ur.getFileName());
        }
        try {
            Files.createDirectories(targetFile.getParent());
            Files.copy(ur.getFile().getInputStream(), targetFile);
        } catch (IOException e) {
            String errorMsg = String.format("Error occurred when saving file with uuid = [%s]", ur);
            log.error(errorMsg, e);
            throw new StorageException(errorMsg, e);
        }

    }

    public void delete(String uuid) {
        File targetDir = basePath.resolve(uuid).toFile();
        FileSystemUtils.deleteRecursively(targetDir);
    }
}
