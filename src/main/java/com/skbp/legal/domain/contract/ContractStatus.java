package com.skbp.legal.domain.contract;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ContractStatus {
    STATUS0("임시저장"),
    STATUS1("검토의뢰서 저장"),
    STATUS2("검퇴의뢰서 결재 중"),
    STATUS3("검토의뢰서 반려"),
    STATUS4("법무팀 반려");

    private String displayName;
}
