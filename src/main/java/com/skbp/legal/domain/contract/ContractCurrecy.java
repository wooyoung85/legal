package com.skbp.legal.domain.contract;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ContractCurrecy {

    KRW("WON"),
    USD("DOLLAR");

    private String displayName;
}
