package com.skbp.legal.domain.contract;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ContractLanguage {
    KR("국문"),
    US("영문"),
    CH("중문"),
    JP("일문"),
    ETC("기타");

    public String displayName;
}
