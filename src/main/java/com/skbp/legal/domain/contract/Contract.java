package com.skbp.legal.domain.contract;

import com.skbp.legal.domain.common.DocumentStatus;
import com.skbp.legal.domain.common.Member;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;


@Getter
@Entity
@EqualsAndHashCode(of = "id")
@Builder @NoArgsConstructor @AllArgsConstructor
@Table(name = "T_CONTRACT")
public class Contract {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String manageNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "member_id")
    private Member requestMember;

    private String name;

    private String companyName;

    private String counterPartner;

    private LocalDate startTermDate;

    private LocalDate endTermDate;

    @Enumerated(EnumType.STRING)
    private ContractCurrecy contractCurrecy;

    private int contractPrice;

    @ElementCollection(fetch = FetchType.EAGER)
    @Enumerated(EnumType.STRING)
    @CollectionTable(name="T_CONTRACT_LANGUAGE")
    private Set<ContractLanguage> contractLanguages;

    private Boolean standardContractUseYn;

    private LocalDate dueDate;

    private LocalDate dealFromDate;

    private LocalDate dealToDate;

    @Enumerated(EnumType.STRING)
    private DocumentStatus documentStatus;

    private String contents;

    private String contentsHtml;

    @Enumerated(EnumType.STRING)
    private ContractStatus contractStatus;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "origin_contract_id")
    private Contract originContract;

    @OneToMany(mappedBy = "originContract")
    private List<Contract> refContracts;

    private LocalDateTime createDt;

    private LocalDateTime updateDt;

    public void setRequestMember(Member member){
        this.requestMember = member;
    }

    public void change(String name) {
        this.name = name;
    }
}
