package com.skbp.legal.domain.common;

import lombok.*;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;


@Getter
@Entity
@EqualsAndHashCode(of = "id")
@Builder @NoArgsConstructor @AllArgsConstructor
@Table(name = "T_MEMBER")
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="member_id")
    private Long id;

    @Column(length = 50, nullable = false)
    private String name;

    @Column(length = 20, nullable = false, unique = true)
    private String email;

    private String deptName;

    @ElementCollection(fetch = FetchType.EAGER)
    @Enumerated(EnumType.STRING)
    @CollectionTable(name="T_MEMBER_ROLE")
    private Set<MemberRole> roles;

    @Column(length = 100, nullable = false)
    private String password;

    private LocalDateTime createdDT;

    private LocalDateTime updatedDT;

    public void setPassword(String encPassword) {
        this.password = encPassword;
    }

}