package com.skbp.legal.domain.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum DocumentStatus {
    NEW("신규"),
    UPDATE("수정"),
    RENEW("갱신");

    public String displayName;
}
