package com.skbp.legal.domain.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.persistence.Table;

@AllArgsConstructor
@Getter
public enum MemberRole {
    ADMIN("ROLE_ADMIN"),
    MEMBER("ROLE_MEMBER");

    private String value;
}