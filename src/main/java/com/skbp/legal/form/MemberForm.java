package com.skbp.legal.form;

import com.skbp.legal.domain.common.MemberRole;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@Getter
@Setter
public class MemberForm {

    @Email
    @NotEmpty(message = "이메일(ID)은 필수입니다.")
    private String email;

    @NotEmpty(message = "이름은 필수 입력 항목입니다.")
    private String name;

    private MemberRole memberRole;

//    @NotEmpty(message = "필수입력 항목")
//    @Pattern(regexp="([a-zA-Z0-9].*[!,@,#,$,%,^,&,*,?,_,~])|([!,@,#,$,%,^,&,*,?,_,~].*[a-zA-Z0-9]){8,12}" ,
//             message="숫자 영문자 특수 문자를 포함한 8 ~ 12 자를 입력하세요. ")
    private String password;

//    @NotEmpty(message = "필수입력 항목")
//    @Pattern(regexp="([a-zA-Z0-9].*[!,@,#,$,%,^,&,*,?,_,~])|([!,@,#,$,%,^,&,*,?,_,~].*[a-zA-Z0-9]){8,12}" ,
//             message="숫자 영문자 특수 문자를 포함한 8 ~ 12 자를 입력하세요. ")
    private String passwordCheck;
}