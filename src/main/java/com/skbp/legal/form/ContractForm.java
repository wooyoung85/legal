package com.skbp.legal.form;

import com.skbp.legal.domain.common.DocumentStatus;
import com.skbp.legal.domain.common.Member;
import com.skbp.legal.domain.common.MemberRole;
import com.skbp.legal.domain.contract.ContractCurrecy;
import com.skbp.legal.domain.contract.ContractLanguage;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.ReadOnlyProperty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Email;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor @NoArgsConstructor
public class ContractForm {

    @NotEmpty(message = "계약명은 필수 입력값입니다.")
    private String name;

    @NotEmpty(message = "회사는 필수 입력값입니다.")
    private String companyName;

    @NotEmpty(message = "계약상대방은 필수 입력값입니다.")
    private String counterPartner;

    @DateTimeFormat(pattern = "yyyyMMdd")
    private LocalDate startTermDT;

    @DateTimeFormat(pattern = "yyyyMMdd")
    private LocalDate endTermDT;

    private ContractCurrecy contractCurrecy;

    private int contractPrice;

    private Set<ContractLanguage> contractLanguages;

    private Boolean standardContractUseYn;

    private LocalDate dueDate;

    private LocalDate dealFromDate;

    private LocalDate dealToDate;

    @Enumerated(EnumType.STRING)
    private DocumentStatus documentStatus;

    private String contents;

    private String contentsHtml;
}