package com.skbp.legal.controller.rest;

import com.skbp.legal.common.AuthUser;
import com.skbp.legal.controller.resource.UploadRequest;
import com.skbp.legal.controller.resource.FormResponse;
import com.skbp.legal.controller.resource.UploadResponse;
import com.skbp.legal.domain.common.Member;
import com.skbp.legal.form.ContractForm;
import com.skbp.legal.service.ContractService;
import com.skbp.legal.service.FileStorageService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class ContractRestController {

    private final ContractService contractService;
    private final FileStorageService fileStorageService;

    @PostMapping("/api/contract/new")
    public ResponseEntity<FormResponse> createContractRequest(@Valid @RequestBody ContractForm form,
                                                              Errors errors,
                                                              @AuthUser Member currentUser) {
        if (errors.hasErrors()) {
            return badRequest(errors);
        }

        contractService.createContractRequest(form, currentUser);
        return ResponseEntity.ok(new FormResponse(true));
    }

    @PostMapping("/api/upload/{menuId}/{type}")
    public ResponseEntity<UploadResponse> upload(
        @RequestParam("qqfile") MultipartFile file,
        @RequestParam(value = "qqfilename") String fileName,
        @RequestParam(value = "qquuid") String uuid,
        @RequestParam(value = "qqpartindex", required = false, defaultValue = "-1") int partIndex,
        @RequestParam(value = "qqtotalparts", required = false, defaultValue = "-1") int totalParts,
        @RequestParam(value = "qqtotalfilesize", required = false, defaultValue = "-1") long totalFileSize,
        @PathVariable("menuId") String menuId,
        @PathVariable("type") String type
    ) {
        System.out.println(type);
        UploadRequest request = new UploadRequest(uuid, file);
        request.setFileName(fileName);
        request.setTotalFileSize(totalFileSize);
        request.setPartIndex(partIndex);
        request.setTotalParts(totalParts);

        fileStorageService.save(request);

        return ResponseEntity.ok().body(new UploadResponse(true));
    }

    @DeleteMapping("/api/upload/{uuid}")
    public ResponseEntity<Void> delete(@PathVariable("uuid") String uuid) {
        fileStorageService.delete(uuid);
        return ResponseEntity.ok().build();
    }

    private ResponseEntity badRequest(Errors errors) {
        return ResponseEntity.badRequest().body(new FormResponse(false, errors));
    }
}
