package com.skbp.legal.controller;

import com.skbp.legal.domain.common.Member;
import com.skbp.legal.domain.common.MemberRole;
import com.skbp.legal.form.MemberForm;
import com.skbp.legal.repository.MemberRepository;
import com.skbp.legal.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.security.Principal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

@Controller
@RequiredArgsConstructor
public class MemberController {

    private final MemberService memberService;
    private final MemberRepository memberRepository;

    @GetMapping("/members/new")
    public String createMemberForm(Model model) {
        model.addAttribute("memberForm", new MemberForm());
        model.addAttribute("memberRoles", MemberRole.values());

        return "members/createMemberForm";
    }

    @PostMapping("/members/new")
    public String createMember(@Valid MemberForm memberForm, BindingResult result) {
        if (result.hasErrors()) {
            return "members/createMemberForm";
        }
        Set<MemberRole> roles = new HashSet<>();
        roles.add(memberForm.getMemberRole());

        Member member = Member.builder()
            .email(memberForm.getEmail())
            .name(memberForm.getName())
            .roles(roles)
            .password(memberForm.getPassword())
            .build();

        memberService.join(member);
        return "redirect:/members/login";
    }

    // 로그인 페이지
    @GetMapping("/members/login")
    public String login() {
        return "members/loginForm";
    }

    // 접근 거부 페이지
    @GetMapping("/members/denied")
    public String dispDenied() {
        return "/denied";
    }

    // 내 정보 페이지
    @GetMapping("/members/myPage")
    public String myPage(Principal principal, Model model) {
        model.addAttribute("member", memberRepository.findByEmail(principal.getName()).get(0));
        return "members/myPage";
    }

    // 어드민 페이지
    @GetMapping("/admin")
    public String dispAdmin() {
        return "/admin";
    }
}
