package com.skbp.legal.controller.resource;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class UploadRequest {

    private final String uuid;
    private final MultipartFile file;
    private int partIndex = -1;
    private long partSize = -1;
    private int totalParts = -1;
    private long totalFileSize = -1;
    private String fileName;

    public UploadRequest(String uuid, MultipartFile file) {
        this.uuid = uuid;
        this.file = file;
    }

    @Override
    public String toString() {
        return "UploadRequest{" +
            "uuid='" + uuid + '\'' +
            ", partIndex=" + partIndex +
            ", partSize=" + partSize +
            ", totalParts=" + totalParts +
            ", totalFileSize=" + totalFileSize +
            ", fileName='" + fileName + '\'' +
            '}';
    }
}