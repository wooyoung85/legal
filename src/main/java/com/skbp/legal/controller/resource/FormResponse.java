package com.skbp.legal.controller.resource;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class FormResponse {
    @JsonProperty("error")
    private List<FieldError> errorMsg;

    private boolean success;

    public FormResponse(boolean success) {
        this.success = success;
    }

    public FormResponse(boolean success, Errors errors){
        this.success = success;
        this.errorMsg = errors.getFieldErrors();
    }
}
