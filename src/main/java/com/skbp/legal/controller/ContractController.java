package com.skbp.legal.controller;

import com.skbp.legal.common.AuthUser;
import com.skbp.legal.domain.common.Member;
import com.skbp.legal.domain.contract.Contract;
import com.skbp.legal.form.ContractForm;
import com.skbp.legal.service.ContractService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class ContractController {

    private final ContractService contractService;

    @GetMapping("/contract/request")
    public String contractRequestForm(Model model, @AuthUser Member authUser) {
        model.addAttribute("contractRequestForm", new ContractForm());
        model.addAttribute("authUser", authUser);
        return "contract/contractRequestForm";
    }

    @GetMapping("/contract/view/{id}")
    public String contractView(@PathVariable("id") Long id, Model model, @AuthUser Member authUser) {

        model.addAttribute("contract", contractService.findOne(id));
        return "contract/contractView";
    }

    @GetMapping("/contract")
    public String contractList(Model model){
        model.addAttribute("contractList", contractService.findContractRequests());
        return "contract/contractList";
    }

}
