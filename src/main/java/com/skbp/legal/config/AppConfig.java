package com.skbp.legal.config;

import com.skbp.legal.domain.common.Member;
import com.skbp.legal.domain.common.MemberRole;
import com.skbp.legal.repository.ContractRepository;
import com.skbp.legal.service.ContractService;
import com.skbp.legal.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Configuration
public class AppConfig {
    @Bean
    public ApplicationRunner applicationRunner() {
        return new ApplicationRunner() {
            @Autowired
            MemberService memberService;

            @Autowired
            ContractRepository contractRepository;

            @Autowired
            AppProperties appProperties;

            @Override
            public void run(ApplicationArguments args) throws Exception {
                Set<MemberRole> roles = new HashSet<>();
                roles.add(MemberRole.MEMBER);
                roles.add(MemberRole.ADMIN);

                Member admin = Member.builder()
                    .email(appProperties.getAdminUsername())
                    .name("어드민")
                    .deptName("법무팀")
                    .roles(roles)
                    .password(appProperties.getAdminPassword())
                    .createdDT(LocalDateTime.now())
                    .build();

                memberService.join(admin);

                Member user = Member.builder()
                    .email(appProperties.getUserUsername())
                    .name("사용자")
                    .deptName("현업팀")
                    .roles(roles)
                    .password(appProperties.getUserPassword())
                    .createdDT(LocalDateTime.now())
                    .build();

                memberService.join(user);

                contractRepository.updateRequestMember(user);
            }
        };
    }
}
