package com.skbp.legal.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.nio.file.Paths;

@Data
@Component
@ConfigurationProperties(prefix = "storage")
public class FileStorageProperties {
	private Path baseDir = Paths.get("./uploads");
}
