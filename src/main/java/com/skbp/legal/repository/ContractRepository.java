package com.skbp.legal.repository;

import com.skbp.legal.domain.common.Member;
import com.skbp.legal.domain.contract.Contract;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class ContractRepository {

    private final EntityManager em;

    public void save(Contract contract){
        em.persist(contract);
    }

    public Contract findOne(Long id){
        return em.find(Contract.class, id);
    }

    public List<Contract> findAll(){
        return em.createQuery("select c from Contract c", Contract.class)
            .getResultList();
    }

    // 테스트 데이터를 위해 만든 기능
    @Transactional
    public void updateRequestMember(Member member) {
        String jpql = "UPDATE Contract con " +
                      "SET con.requestMember = :member ";

        em.createQuery(jpql)
            .setParameter("member", member)
            .executeUpdate();
    }

    @Transactional
    public int getMaxId() {
        String jpql = "SELECT max(id) FROM Contract ";
        Object singleResult = em.createQuery(jpql).getSingleResult();
        return singleResult != null ? ((Long) singleResult).intValue() : 0;
    }
}
