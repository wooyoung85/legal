package com.skbp.legal.domain.contract;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QContract is a Querydsl query type for Contract
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QContract extends EntityPathBase<Contract> {

    private static final long serialVersionUID = 1778202068L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QContract contract = new QContract("contract");

    public final StringPath companyName = createString("companyName");

    public final StringPath contents = createString("contents");

    public final StringPath contentsHtml = createString("contentsHtml");

    public final EnumPath<ContractCurrecy> contractCurrecy = createEnum("contractCurrecy", ContractCurrecy.class);

    public final SetPath<ContractLanguage, EnumPath<ContractLanguage>> contractLanguages = this.<ContractLanguage, EnumPath<ContractLanguage>>createSet("contractLanguages", ContractLanguage.class, EnumPath.class, PathInits.DIRECT2);

    public final NumberPath<Integer> contractPrice = createNumber("contractPrice", Integer.class);

    public final EnumPath<ContractStatus> contractStatus = createEnum("contractStatus", ContractStatus.class);

    public final StringPath counterPartner = createString("counterPartner");

    public final DateTimePath<java.time.LocalDateTime> createDt = createDateTime("createDt", java.time.LocalDateTime.class);

    public final DatePath<java.time.LocalDate> dealFromDate = createDate("dealFromDate", java.time.LocalDate.class);

    public final DatePath<java.time.LocalDate> dealToDate = createDate("dealToDate", java.time.LocalDate.class);

    public final EnumPath<com.skbp.legal.domain.common.DocumentStatus> documentStatus = createEnum("documentStatus", com.skbp.legal.domain.common.DocumentStatus.class);

    public final DatePath<java.time.LocalDate> dueDate = createDate("dueDate", java.time.LocalDate.class);

    public final DatePath<java.time.LocalDate> endTermDate = createDate("endTermDate", java.time.LocalDate.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath manageNumber = createString("manageNumber");

    public final StringPath name = createString("name");

    public final QContract originContract;

    public final ListPath<Contract, QContract> refContracts = this.<Contract, QContract>createList("refContracts", Contract.class, QContract.class, PathInits.DIRECT2);

    public final com.skbp.legal.domain.common.QMember requestMember;

    public final BooleanPath standardContractUseYn = createBoolean("standardContractUseYn");

    public final DatePath<java.time.LocalDate> startTermDate = createDate("startTermDate", java.time.LocalDate.class);

    public final DateTimePath<java.time.LocalDateTime> updateDt = createDateTime("updateDt", java.time.LocalDateTime.class);

    public QContract(String variable) {
        this(Contract.class, forVariable(variable), INITS);
    }

    public QContract(Path<? extends Contract> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QContract(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QContract(PathMetadata metadata, PathInits inits) {
        this(Contract.class, metadata, inits);
    }

    public QContract(Class<? extends Contract> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.originContract = inits.isInitialized("originContract") ? new QContract(forProperty("originContract"), inits.get("originContract")) : null;
        this.requestMember = inits.isInitialized("requestMember") ? new com.skbp.legal.domain.common.QMember(forProperty("requestMember")) : null;
    }

}

